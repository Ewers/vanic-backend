import jwt 		from 'jsonwebtoken';
import aguid 	from 'aguid';
import Db 		from '../db';
import Boom 	from 'boom';
import Bcrypt from 'bcrypt';
import Joi    from 'joi';
var redisClient = require('redis-connection')();

redisClient.set('redis', 'working');
redisClient.get('redis', (rediserror, reply) => {
  /* istanbul ignore if */
  if(rediserror) {
    console.log(rediserror);
  }
  console.log('redis is ' +reply.toString()); // confirm we can access redis
});

const cookie_options = {
  ttl: 365 * 24 * 60 * 60 * 1000, // expires a year from today
  encoding: 'none',    // we already used JWT to encode
  isSecure: false,      // warm & fuzzy feelings
  isHttpOnly: true,    // prevent client alteration
  clearInvalid: false, // remove invalid cookies
  strictHeader: true   // don't allow violations of RFC 6265
};

export default [{

  method: ['GET', 'POST'],
  path: "/auth",

  config: {
    auth: false
  },
  handler: (request, reply) => {

    const {username, password} = request.payload;

    Db.models.user.findOne({where: {username: username}}).then((user) => {

    	// do your checks to see if the person is valid
    	if (!user) {
    		reply(Boom.badRequest('No user found'));
    	}

      const session = {
        valid: true, // this will be set to false when the person logs out
        id: aguid(), // a random session id
        exp: new Date().getTime() + 30 * 60 * 1000,
        userId: user.dataValues.id // expires in 30 minutes time
      };

	    Bcrypt.compare(password, user.dataValues.password, (err, isValid) => {
	    	console.log(isValid);
	        if(isValid){

  			    redisClient.set(session.id, JSON.stringify(session));
  			    // sign the session as a JWT
  			    const token = jwt.sign(session, process.env.JWT_SECRET); // synchronous
            reply({successMessage: 'You have been authenticated!', isAuthenticated: true, userId: session.userId})
            .header("Authorization", token)        // where token is the JWT
            .state("token", token);
	        } else {
	        	reply(Boom.unauthorized('Wrong password'));
	        }
	    });
    });
  }
},
{
  method: ['POST'],
  path: "/auth/register",
  config: {
    auth: false,
    validate: {
        payload: {
            email: Joi.string().email().required(),
            username: Joi.string().alphanum().min(4).max(30).required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required()
        }
    }
  },
  handler: (request, reply) => {

    const {email, username, password} = request.payload;

    Db.models.user.create({
      username: username,
      email: email,
      password: password
    }).then(user => {
      return user.createTask({
          title: 'title',
          project: 'project',
          description: 'description',
          tags: 'tags'
    }, {
      include: [Db.models.user]
    }).then( task => {
        return reply({message: 'Logged in!', isAuthenticated: false, userId: user.dataValues.id});
      });
    });
  }
},
{
  method: ['GET', 'POST'],
  path: "/logout",
  config: {
    auth: 'jwt'
  },
  handler: (request, reply) => {
    // implement your own login/auth function here
        // implement your own login/auth function here
      jwt.verify(request.auth.token,
        process.env.JWTSECRET, (err, decoded) => {

          console.log(decoded);

          if(err){
            reply(Boom.badRequest(err));
          };

          let session;
          redisClient.get(decoded.id,(rediserror, redisreply) => {
            /* istanbul ignore if */
            if(rediserror) {
              console.log(rediserror);
            }
            session = JSON.parse(redisreply)
            console.log(' - - - - - - SESSION - - - - - - - -')
            console.log(session);
            // update the session to no longer valid:
            session.valid = false;
            session.ended = new Date().getTime();
            // create the session in Redis
            redisClient.set(session.id, JSON.stringify(session));

            reply({text: 'Check Auth Header for your Token'})
          });

        });
      }
}];