import auth from './auth';
import home from './home';
import assets from './assets';

export default [].concat(auth, home, assets);
