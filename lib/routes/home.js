import req from 'request';

export default [{
  method: 'GET',
  path: "/",
  config: {
    auth: false
  },
  handler: (request, reply) => {

  	if(process.env.NODE_ENV === 'development'){
		req('http://localhost:3000/', function (error, response, body) {
		  if (!error && response.statusCode == 200) {
		  	let indexApp = body.search('app');
		  	let hashApp = body.substring(indexApp + 4, indexApp + 24);
        console.log(hashApp);
		      reply.view('index', {
		      	hash: hashApp
		      })
		  }
		});
  	} else {
      reply.view('index');
  	}
  }
},
{
  method: 'GET',
  path: "/login",
  config: {
    auth: false
  },
  handler: (request, reply) => {

    if(process.env.NODE_ENV === 'development'){
    req('http://localhost:3000/', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        let indexApp = body.search('app');
        let hashApp = body.substring(indexApp + 4, indexApp + 24);
          reply.view('index', {
            hash: hashApp
          })
      }
    });
    } else {
      reply.view('index');
    }
  }
},
{
  method: 'GET',
  path: "/register",
  config: {
    auth: false
  },
  handler: (request, reply) => {

    if(process.env.NODE_ENV === 'development'){
    req('http://localhost:3000/', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        let indexApp = body.search('app');
        let hashApp = body.substring(indexApp + 4, indexApp + 24);
          reply.view('index', {
            hash: hashApp
          })
      }
    });
    } else {
      reply.view('index');
    }
  }
},
{
  method: 'GET',
  path: "/app/{param*}",
  config: {
    auth: 'login'
  },
  handler: (request, reply) => {

    if(request.auth.token == undefined){
      reply.redirect('/login');
    }

    if(process.env.NODE_ENV === 'development'){
    req('http://localhost:3000/', function (error, response, body) {
      if (!error && response.statusCode == 200) {
        let indexApp = body.search('app');
        let hashApp = body.substring(indexApp + 4, indexApp + 24);
          reply.view('index', {
            hash: hashApp
          })
      }
    });
    } else {
      reply.view('index');
    }
  }
}];