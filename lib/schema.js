import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
} from 'graphql';

import Db from './db';

const Task = new GraphQLObjectType({
  name: 'Task',
  description: 'User task',
  fields () {
    return {
      title: {
        type: GraphQLString,
        resolve (task) {
          return task.title;
        }
      },
      project: {
        type: GraphQLString,
        resolve (task) {
          return task.project;
        }
      },
      description: {
        type: GraphQLString,
        resolve (task) {
          return task.description;
        }
      },
      tags: {
        type: GraphQLString,
        resolve (task) {
          return task.tags;
        }
      },
      userId: {
        type: GraphQLString,
        resolve (task) {
          return task.userId;
        }
      },
      user: {
        type: User,
        resolve (task) {
          return task.getUsers();
        }
      }
    };
  }
});

const User = new GraphQLObjectType({
  name: 'User',
  description: 'This represents a User',
  fields: () => {
    return {
      id: {
        type: GraphQLString,
        resolve (user) {
          return user.id;
        }
      },
      username: {
        type: GraphQLString,
        resolve (user) {
          return user.username;
        }
      },
      email: {
        type: GraphQLString,
        resolve (user) {
          return user.email;
        }
      },
      tasks: {
        type: new GraphQLList(Task),
        resolve (user) {
          return user.getTasks();
        }
      }
    };
  }
});

const Query = new GraphQLObjectType({
  name: 'Query',
  description: 'Root query object',
  fields: () => {
    return {
      user: {
        type: User,
        args: {
          id: {
            type: GraphQLString
          }
        },
        resolve (root, args) {
          return Db.models.user.findOne({ where: args });
        }
      },
      task: {
        type: Task,
        args: {
          project: {
            type: GraphQLString
          }
        },
        resolve (root, args) {
          return Db.models.task.findOne({ where: args });
        }
      },
      tasks: {
        type: new GraphQLList(Task),
        resolve (root, args) {
          return Db.models.task.findAll({ where: args });
        }
      }
    };
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutations',
  description: 'Functions to set stuff',
  fields () {
    return {
      addTask: {
        type: Task,
        args: {
          title: {
            type: new GraphQLNonNull(GraphQLString)
          },
          project: {
            type: new GraphQLNonNull(GraphQLString)
          },
          description: {
            type: new GraphQLNonNull(GraphQLString)
          },
          tags: {
            type: new GraphQLNonNull(GraphQLString)
          },
          userId: {
            type: new GraphQLNonNull(GraphQLString)
          }
        },
        resolve (source, args) {
          return Db.models.task.create({
            title: args.title,
            project: args.project,
            description: args.description,
            tags: args.tags,
            userId: args.userId
          }, {
            include: [Db.models.user]
          });
        }
      }
    };
  }
});

const Schema = new GraphQLSchema({
  query: Query,
  mutation: Mutation
});

export default Schema;