require('env2')('config.env');

require('babel-core/register')({
    presets: ['react', 'es2015', 'stage-0']
});

import 'babel-polyfill';

import Path     from 'path';
import Hapi     from 'hapi';
import Inert    from 'inert';
import Vision   from 'vision';
import routes   from './routes';
import Boom     from 'boom';
import Tv       from 'tv';
import Yar      from 'yar';
import GraphQL  from 'hapi-graphql';


const server = new Hapi.Server({
  debug: { request: ['error'] },
  connections: {
      routes: {
          files: {
              relativeTo: Path.join(__dirname, '../assets')
          }
      }
  }
});

server.connection({
    host: 'localhost',
    port: process.env.PORT
});

server.state('data', {
    ttl: null,
    isSecure: process.env.NODE_ENV == 'development' ? false : true,
    isHttpOnly: true,
    encoding: 'base64json',
    clearInvalid: false, // remove invalid cookies
    strictHeader: true // don't allow violations of RFC 6265
});

import Schema from './schema';

const graphqlOptions = {
  register: GraphQL,
  options: {
    query: {
      schema: Schema,
      rootValue: {},
      pretty: false
    },
    route: {
      path: '/graphql'
    }
  }
}

const tvOptions = {
  endpoint: '/debug/console'
};

const goodOptions = {
    opsInterval: 1000,
    reporters: [{
        reporter: require('good-console'),
        events: { log: '*', response: '*' }
    }, {
        reporter: require('good-file'),
        events: { ops: '*' },
        config: './test/fixtures/awesome_log'
    }, {
        reporter: 'good-http',
        events: { error: '*' },
        config: {
            endpoint: 'http://prod.logs:3000',
            wreck: {
                headers: { 'x-api-key' : 12345 }
            }
        }
    }]
};

const yarOptions = {
    storeBlank: false,
    cookieOptions: {
        password: 'password',
        isSecure: process.env.NODE_ENV !== 'development'
    }
};

import HapiJwt from 'hapi-auth-jwt2';
import validate from './validate';

server.register([
  graphqlOptions,
  HapiJwt,
  {
    register: require('good'),
    options: goodOptions
  },
  {
    register: Yar,
    options: yarOptions
  },
  Inert,
  Vision,
  {
    register: Tv,
    options: tvOptions
  }], (err) => {

    if (err) {
      console.log(err + 'Failed to load plugins.');
    }

    server.auth.scheme('login', function (server, options) {
        return {
            authenticate: function (request, reply) {
                server.auth.test('jwt', request, function (err, credentials) {
                    if (err) {
                        reply.redirect('/login');
                    }
                    else {
                        reply.continue({ credentials: credentials });
                    }
                });
            }
        };
    });

    server.auth.strategy("login", "login");

    server.auth.strategy('jwt', 'jwt',
    { key: process.env.JWT_SECRET, // Never Share your secret key
      validateFunc: validate,      // validate function defined above
      verifyOptions: {
        ignoreExpiration: false,    // do not reject expired tokens
        algorithms: [ 'HS256' ]    // specify your secure algorithm
      }
    });

    server.auth.default({
        strategies: ['jwt', 'login']
    });

    server.views({
    engines: {
        html: require('ejs')
    },
    relativeTo: __dirname,
    path: process.env.NODE_ENV === 'development' ? '../views' : '../public'
});

    server.route(routes);

    server.start((err) => {

        if (err) {
          throw err;
        }
        console.log('--------------------------------------------------------------------------------' );
        console.log(' 😀😀😀😀   Server is listening at ' + server.info.uri + '   😀😀😀😀');
        console.log('--------------------------------------------------------------------------------' );
    });
});
