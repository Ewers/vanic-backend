export default {
    method: 'GET',
    path: '/assets/{param*}',
	config: {
	auth: false
	},
    handler: {
        directory: {
            path: '../public',
            listing: true
        }
    }
};