import Sequelize from 'sequelize';
import bcrypt from 'bcrypt';

const Conn = new Sequelize(
  'ewers',
  'postgres',
  'postgres',
  {
    dialect: 'postgres',
    host: 'localhost'
  }
);

const User = Conn.define('user', {
  id: {
    type: Sequelize.UUID,
    primaryKey: true,
    defaultValue: Sequelize.UUIDV4,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    set:  function(v) {
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(v, salt);

      this.setDataValue('password', hash);
    }
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    validate: {
      isEmail: true
    }
  }
});

const Task = Conn.define('task', {
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  project: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.STRING,
    allowNull: false
  },
  tags: {
    type: Sequelize.STRING,
    allowNull: false
  }
});

// Relations
User.hasMany(Task);
Task.belongsTo(User);

Conn.sync();

export default Conn;